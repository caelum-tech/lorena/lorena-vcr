# Lorena VCR
Implementation of the [Lorena](https://gitlab.com/caelum-tech/lorena/lorena-sdk/blob/master/src/intro.md) [Verifiable Credentials Request standard](https://gitlab.com/caelum-tech/lorena/lorena-sdk/blob/master/src/verifiable-credential-request-standard.md)

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/Lorena/lorena-vcr/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/Lorena/lorena-vcr/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-vcr/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/Lorena/lorena-vcr/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-vcr/commits/master)|

## Run tests

Install dependencies
```bash
npm i
```

Run the tests
```bash
npm test
```

## Usage
```bash
npm install --save-dev @caelum-tech/lorena-vcr

```

```JavaScript
const verifierClient = require('@caelum-tech/lorena-vcr')
```

## See also
[Lorena](https://gitlab.com/caelum-tech/lorena/lorena-sdk/blob/master/src/intro.md)
