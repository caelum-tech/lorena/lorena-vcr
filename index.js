
const { decodeVCR } = require('./src/decodeVCR.js')
const { encodeVCR, vcrBody } = require('./src/encodeVCR.js')

module.exports = { decodeVCR, encodeVCR, vcrBody }
