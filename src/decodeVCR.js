const atob = require('atob')
const Encodr = require('encodr')

/**
 * Parse a Verifiable Credential Request URL
 * @param {String} url - VCR URL
 * @param optional {String} param - parameter which holds the body
 * @returns {Object} - an object representation of the parsed JSON
 */
function decodeVCR (url, param = 'vcr') {
  const theURL = new URL(url)
  const vcr = theURL.searchParams.get(param)
  const body = atob(vcr)
  let json = body
  if (body[0] !== '{') {
    const cbor = new Encodr('cbor')
    json = cbor.decode(body)
  }
  return JSON.parse(json)
}

module.exports = { decodeVCR }
