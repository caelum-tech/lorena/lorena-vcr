const btoa = require('btoa')
const Encodr = require('encodr')

/**
 * Creates the body of a Verifiable Credentials Request
 * @param {String} transportType - for Matrix, 'matrix-guest' or 'matrix-user'
 * @param {String} transportAddress - the endpoint for the request; for Matrix, a user ID
 * @param {String} type - a URI which is either a URN or URL for the credential type requested
 * @param {String} identity - the DID of the entity requesting the credential
 * @returns {Object} - a Javascript object with the attributes specified
 */
function vcrBody (transportType, transportAddress, type, identity) {
  return {
    transport: {
      type: transportType,
      address: transportAddress
    },
    type,
    identity
  }
}

/**
 * Encode a Verifiable Credential Request URL
 * @param {String} baseUrl - the URL at the beginning
 * @param {Object} body - the Javascript object with the attributes to be included
 * @param optional {String} encodeType - 'json' (default) or 'cbor'
 * @returns {String} - the VCR URL
 */
function encodeVCR (baseUrl, body, encodeType = 'json') {
  var encodedBody
  if (encodeType === 'json') {
    const JSON = new Encodr('json')
    encodedBody = btoa(JSON.encode(body))
  } else if (encodeType === 'cbor') {
    const CBOR = new Encodr('cbor')
    encodedBody = btoa(CBOR.encode(body).toString('binary'))
  } else {
    // console.log('Invalid encodeType: ', encodeType)
    return ''
  }

  // construct URL
  return `${baseUrl}/?vcr=${encodedBody}`
}

module.exports = { encodeVCR, vcrBody }
