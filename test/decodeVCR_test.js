const chai = require('chai')
const helpers = require('../index.js')

// Configure chai
chai.should()
const expect = chai.expect

describe('DecodeVCR', function () {
  const baseUrl = 'https://playground.did'
  const transportType = 'matrix-guest'
  const transportAddress = '@guest101:matrix.org'
  const vcType = 'lor:EmailCredential'
  const requestorDID = 'did:lor:third-party'
  const body = helpers.vcrBody(transportType, transportAddress, vcType, requestorDID)

  // something's broken in CBOR
  xit('should parse CBOR URL', async () => {
    const cborRequest = helpers.encodeVCR(baseUrl, body, 'cbor')
    const cborBody = helpers.decodeVCR(cborRequest)
    cborBody.transport.type.should.eq(transportType)
    cborBody.transport.address.should.eq(transportAddress)
    cborBody.type.should.eq(vcType)
    cborBody.identity.should.eq(requestorDID)
  })

  it('should parse JSON URL', async () => {
    const jsonRequest = helpers.encodeVCR(baseUrl, body)
    const jsonBody = helpers.decodeVCR(jsonRequest)
    jsonBody.transport.type.should.eq(transportType)
    jsonBody.transport.address.should.eq(transportAddress)
    jsonBody.type.should.eq(vcType)
    jsonBody.identity.should.eq(requestorDID)
  })

  it('should not parse invalid URL', async () => {
    const badURL = 'https.playground.did/?vcr=o2l0'
    expect(() => { helpers.decodeVCR(badURL) }).to.throw(TypeError)
  })

  it('should not parse URL without vcr param', async () => {
    const jsonRequest = 'https://playground.did/?request=o2l0cmFuc3BvcnSiZHR5cGVsbWF0cml4LWd1ZXN0Z2FkZHJlc3NzQDMxNzAyODk6bWF0cml4Lm9yZ2R0eXBlc2xvcjpFbWFpbENyZWRlbnRpYWxoaWRlbnRpdHlzZGlkOmxvcjp0aGlyZC1wYXJ0eQ=='
    expect(() => { helpers.decodeVCR(jsonRequest) }).to.throw(TypeError)
  })

  it('should decode URL with custom param', async () => {
    const jsonRequest = helpers.encodeVCR(baseUrl, body, 'json').replace('?vcr=', '?request=')
    const jsonBody = helpers.decodeVCR(jsonRequest, 'request')
    jsonBody.should.not.be.empty
    jsonBody.type.should.eq(vcType)
  })
})
