const chai = require('chai')
const helpers = require('../index.js')

// Configure chai
chai.should()
chai.use(require('chai-string'))

describe('VCR Body', function () {
  it('should make a VCR body', async () => {
    const body = helpers.vcrBody('transport type', 'transport address', 'uri:something', 'did:some:one:call:batman')
    body.transport.type.should.eq('transport type')
    body.transport.address.should.eq('transport address')
    body.type.should.eq('uri:something')
    body.identity.should.eq('did:some:one:call:batman')
  })
})

describe('Encode VCR', function () {
  const baseUrl = 'https://playground.did'
  const transportType = 'matrix-guest'
  const transportAddress = '@guest101:matrix.org'
  const vcType = 'lor:EmailCredential'
  const requestorDID = 'did:lor:third-party'
  const body = helpers.vcrBody(transportType, transportAddress, vcType, requestorDID)

  it('should create credential request with CBOR format', async () => {
    const credentialRequestCbor = helpers.encodeVCR(baseUrl, body, 'cbor')
    credentialRequestCbor.should.not.be.empty
    credentialRequestCbor.should.startWith(baseUrl)
  })

  it('should create credential request with JSON format', async () => {
    const credentialRequestJson = helpers.encodeVCR(baseUrl, body, 'json')
    credentialRequestJson.should.not.be.empty
    credentialRequestJson.should.startWith(baseUrl)
  })

  it('should not create credential request with invalid format', async () => {
    const credentialRequestInvalid = helpers.encodeVCR(baseUrl, body, 'invalid')
    credentialRequestInvalid.should.be.empty
  })
})
