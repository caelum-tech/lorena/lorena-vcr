const index = require('../index.js')
const chai = require('chai')

// Configure chai
chai.should()

describe('Library', function () {
  it('should export methods', () => {
    index.decodeVCR.should.not.be.undefined
    index.encodeVCR.should.not.be.undefined
    index.vcrBody.should.not.be.undefined
  })
})
